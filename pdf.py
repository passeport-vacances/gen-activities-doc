# Copyright 2016 Jacques Supcik / Passeport Vacances Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess
import jinja2
import os
import re



TEMPLATE = "template2.tex"
OUTPUT_TEX = "preview2.tex"

LATEX_SUBS = (
    (re.compile(r'\\'), r'\\textbackslash'),
    (re.compile(r'([{}_#%&$])'), r'\\\1'),
    (re.compile(r'~'), r'\~{}'),
    (re.compile(r'\^'), r'\^{}'),
    (re.compile(r'"'), r"''"),
    (re.compile(r'\.\.\.+'), r'\\ldots{} '),
    (re.compile(r'\x92'), r"'"),
    (re.compile(r'\x85'), r'\ldots{} '),
    (re.compile(r'\x96'), r'--'),
)


def escape_tex(value):
    newval = value
    for pattern, replacement in LATEX_SUBS:
        newval = pattern.sub(replacement, newval)
    return newval


def cell_break(value):
    value = re.sub(r'\n', r'\\newline{}', value)
    return value


def line_break(value):
    value = re.sub(r'\n', r'\\\n', value)
    return value


def write_pdf(data, dest_dir="."):
    texenv = jinja2.Environment(
        extensions=['jinja2.ext.do'],
        line_statement_prefix="#",
        line_comment_prefix="##",
        loader=jinja2.FileSystemLoader("."))
    texenv.block_start_string = '((*'
    texenv.block_end_string = '*))'
    texenv.variable_start_string = '((('
    texenv.variable_end_string = ')))'
    texenv.comment_start_string = '((='
    texenv.comment_end_string = '=))'
    texenv.filters['escape_tex'] = escape_tex
    texenv.filters['cell_break'] = cell_break
    texenv.filters['line_break'] = line_break

    template = texenv.get_template(TEMPLATE)

    cur_dir = os.curdir
    os.chdir(dest_dir)
    f = open (OUTPUT_TEX, mode="w", encoding="utf-8")
    f.write (template.render(data=data))
    f.close()

    subprocess.call("latexmk -latex='xelatex' {0}".format(OUTPUT_TEX), shell=True)
    subprocess.call("latexmk -latex='xelatex' -c {0}".format(OUTPUT_TEX), shell=True)
    os.chdir(cur_dir)
