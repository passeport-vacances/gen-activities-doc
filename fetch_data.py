import jinja2

import groople
import argparse
import pymysql
import pdf
import pprint

parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("--db_host", type=str, help="Database Host")
parser.add_argument("--db_user", type=str, help="Database Username")
parser.add_argument("--db_password", type=str, help="Database Password")
parser.add_argument("--db_database", type=str, help="Database Name")

parser.add_argument("--debug", help="debug mode")
args = parser.parse_args()

conn = pymysql.connect(
    host=args.db_host,
    user=args.db_user,
    passwd=args.db_password,
    db=args.db_database,
    cursorclass=pymysql.cursors.DictCursor,
)

data = groople.Groople(conn).data()
#pprint.pprint(data)
pdf.write_pdf(data)