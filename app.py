from flask import Flask, request, render_template, send_file
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS
import tempfile
import os.path
import sys
import shutil

import gen_pdf_0

SECRET_KEY = "change-me"
UPLOADED_DUMPS_DEST = './upload'

app = Flask(__name__)
app.config.from_object(__name__)

dumps = UploadSet('dumps', DOCUMENTS)
configure_uploads(app, dumps)

@app.route('/', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST' and 'dumpfile' in request.files:
        path = tempfile.mkdtemp(dir=UPLOADED_DUMPS_DEST)
        filename = dumps.save(request.files['dumpfile'], os.path.basename(path))
        print(filename, file=sys.stderr)
        gen_pdf_0.convert(
            os.path.join(UPLOADED_DUMPS_DEST, filename),
            os.path.join(UPLOADED_DUMPS_DEST, os.path.basename(path)))
        result = send_file(
            os.path.join(UPLOADED_DUMPS_DEST, os.path.basename(path), "preview.pdf"),
            mimetype="application/pdf",
            as_attachment=True,
            attachment_filename="passeport-vacances-fribourg-2016-preview.pdf")

        print (os.curdir, file=sys.stderr)
        print (path, file=sys.stderr)

        # shutil.rmtree(path)
        return result
    return render_template('upload.html')


if __name__ == "__main__":
    app.run(debug=True)
