# Copyright 2016 Jacques Supcik / Passeport Vacances Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess
import xlrd
import hashlib
import pickle
import re
import jinja2
import os

INPUT_XLS = "dump activités.xls"
TEMPLATE = "template.tex"
OUTPUT_TEX = "preview.tex"
GROUP_START = 22 # TODO: Search for "Group" in the header

LATEX_SUBS = (
    (re.compile(r'\\'), r'\\textbackslash'),
    (re.compile(r'([{}_#%&$])'), r'\\\1'),
    (re.compile(r'~'), r'\~{}'),
    (re.compile(r'\^'), r'\^{}'),
    (re.compile(r'"'), r"''"),
    (re.compile(r'\.\.\.+'), r'\\ldots'),
)


def escape_tex(value):
    newval = value
    for pattern, replacement in LATEX_SUBS:
        newval = pattern.sub(replacement, newval)
    return newval


def cell_break(value):
    value = re.sub(r'\n', r'\\newline{}', value)
    return value


def line_break(value):
    value = re.sub(r'\n', r'\\\n', value)
    return value


def cleanup(value):
    value = re.sub(r'\r\n', '\n', value)
    value = value.strip()
    return value


def convert(source, dest_dir):
    book = xlrd.open_workbook(source)
    sheet = book.sheet_by_index(0)
    data = dict()

    # read the heade (first line)
    rows = sheet.get_rows()
    rows_order = list()

    header = [cell.value for cell in rows.__next__()]

    # search the location of the days grid
    (min_d, max_d) = (len(header)+1, -1)
    for i in range(GROUP_START+1,len(header)):
        if re.match(r'^\w{2}\s\d{2}\.\d{2}\s\S+$', header[i]):
            min_d = min(min_d, i)
            max_d = max(max_d, i)

    # parse the data
    for row in rows:
        row = [cleanup(cell.value) for cell in row]
        key = hashlib.sha1(pickle.dumps(row[0:GROUP_START])).hexdigest()
        if key in data:
            record = data[key]
        else:
            record = {"_group":[]}

        for i in range(GROUP_START):
            record[header[i]] = row[i]

        if record["Category"] == "DUMMY":
            continue

        if key not in data:
            rows_order.append(key)

        gr = {"_days":[]}
        for i in range(GROUP_START,len(row)):
            gr[header[i]] = row[i]

        for i in range(min_d, max_d+1):
            if row[i] == "yes":
                gr["_days"].append(header[i])

        group = row[GROUP_START]
        if group in record["_group"]:
            raise Exception("Error")

        # aggregates ages
        ages = row[len(row)-1]
        if ages == "all":
            gr["_ages"] = ages
        else:
            gl = sorted([int(i.strip()) for i in ages.split(",")]) # type: list
            ranges = list()
            while len(gl) > 0:
                min_age = gl.pop(0)
                max_age = min_age
                while len(gl) > 0 and gl[0] == max_age + 1:
                    max_age = gl.pop(0)
                ranges.append((min_age, max_age))
            rs = ["{0}-{1}".format(i[0], i[1]) for i in ranges]
            if len(rs) > 1:
                rs_last = rs.pop()
                gr["_ages"] = " et ".join([", ".join(rs), rs_last])
            else:
                gr["_ages"] = rs[0]

        record["_group"].append(gr)

        data[key] = record

    texenv = jinja2.Environment(loader=jinja2.FileSystemLoader("."))
    texenv.block_start_string = '((*'
    texenv.block_end_string = '*))'
    texenv.variable_start_string = '((('
    texenv.variable_end_string = ')))'
    texenv.comment_start_string = '((='
    texenv.comment_end_string = '=))'
    texenv.filters['escape_tex'] = escape_tex
    texenv.filters['cell_break'] = cell_break
    texenv.filters['line_break'] = line_break

    template = texenv.get_template(TEMPLATE)

    cur_dir = os.curdir
    os.chdir(dest_dir)
    f = open (OUTPUT_TEX, mode="w", encoding="utf-8")
    f.write (template.render(pages=[data[k] for k in rows_order]))
    f.close()

    subprocess.call("latexmk -latex='xelatex' {0}".format(OUTPUT_TEX), shell=True)
    subprocess.call("latexmk -latex='xelatex' -c {0}".format(OUTPUT_TEX), shell=True)
    os.chdir(cur_dir)

if __name__ == '__main__':
    convert(INPUT_XLS, ".")